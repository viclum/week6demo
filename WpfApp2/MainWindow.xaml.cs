﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp2
{
    interface CurrencyLister
    {
        void AddCurrencyName(string name);
    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, CurrencyLister
    {
        private ConvertCurrencyCTL controlObj;
        public MainWindow()
        {
            InitializeComponent();
            controlObj = new ConvertCurrencyCTL(this);
            //this.fromChoice.Items.Add("a");
        }

        public void AddCurrencyName(string name)
        {
            fromChoice.Items.Add(name);
            toChoice.Items.Add(name);
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            toTextBox.Text = controlObj.Convert(Convert.ToDouble(fromTextBox.Text), fromChoice.Text, toChoice.Text).ToString();
        }
    }
}
