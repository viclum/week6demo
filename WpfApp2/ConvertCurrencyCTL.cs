﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp2
{
    class ConvertCurrencyCTL
    {
        private ListCurrenciesCTL obj;
        public ConvertCurrencyCTL(CurrencyLister form)
        {
            obj = new ListCurrenciesCTL();
            obj.listAllCurrencies(form);
        }

        public double Convert(double amount, string from, string to)
        {
            double result = amount * obj.Rates[to] / obj.Rates[from];
            return result;
        }
    }
}
