using Newtonsoft.Json;
//using System.Text.Json;
//using System.Text.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp2
{
    class ListCurrenciesCTL
    {
        public Dictionary<string, double> Rates;
        public async void listAllCurrencies(CurrencyLister ccui)
        {
            await GetData();
            foreach (var c in Rates.Keys)
            {
                ccui.AddCurrencyName(c);
            }
        }
        private async Task GetData()
        {
            HttpClient client = new HttpClient();
			// https://fixer.io/documentation
			/*
			Go to fixer.io and create an account. After signup, you will receive an access token immediately. 
			If you plan on using less than 1000 requests per month, your account will be completely free. 
			If you need more or want to use some of our new features, you’ll need to choose one of the paid options
			*/
            client.BaseAddress = new Uri("http://data.fixer.io");
            HttpResponseMessage response = await client.GetAsync("/api/latest?access_key=YOUR_ACCESS_KEY");
            if (response.IsSuccessStatusCode)
            {
                string data = await response.Content.ReadAsStringAsync();
                Result result = JsonConvert.DeserializeObject<Result>(data);

                Rates = result.Rates;
            }
        }

    }
    public class Result
    {
        public bool Success { get; set; }
        public int Timestamp { get; set; }
        public string Base { get; set; }
        public DateTime Date { get; set; }
        public Dictionary<string, double> Rates = new Dictionary<string, double>();
    }
}
